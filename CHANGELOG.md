## 0.2.2-beta (December 14, 2020)

### Core

- Add RecoursiveShape
- Move to 'style' the properties used by drawer
- Rename RegularPolygon to Polygon

## 0.1.0-alpha.2 (November 12, 2020)

### Core

- In the generation of the shapes the 'squeeze' was moved before apply the transformations
